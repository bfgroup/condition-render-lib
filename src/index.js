import React from 'react'

const Condition = (props) => {

    const requiredChildrenProp = 'id'
    const isArrayChildren = Array.isArray(props.children)
    const isObjectChildren = props.children instanceof Object && !isArrayChildren

    const renderArrayChild = () => {
        const arrayOfChildren = props.children

        return arrayOfChildren.map((child) => {
            const isArrayChild = Array.isArray(child)
            const isObjectChild = child instanceof Object && !isArrayChild
            if (isObjectChild) {
                return checkChildToRequiredProp(child)
            }
            return null
        })
    }

    const checkChildToRequiredProp = (childElement) => {
        const elementProps = childElement.props

        if (elementProps?.hasOwnProperty(requiredChildrenProp)) {
            /// if childElement has 'id' prop
            const isEqualRequiredProps = props.activeId === elementProps[requiredChildrenProp]

            if (isEqualRequiredProps) {
                /// if activeId === id of a childElement
                return childElement
            } else {
                return null
            }

        } else {
            throw new Error('When using a condition component, you must specify the "id" property for all its child components.')
        }
    }

    if (props.children) {

        if (isArrayChildren) {
            /// If child is an Array
            return (
                <>
                    { renderArrayChild() }
                </>
            )
        }

        if (isObjectChildren) {
            /// If a child component is only one and it's an Array
            const childElement = props.children
            return checkChildToRequiredProp(childElement)
        }
    }

    return null
}

export default Condition